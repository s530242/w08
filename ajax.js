(function ($) {
    $('#btnLoadText').click(function () { $("#showResult").load("show.txt"); });
    $('#btnAjax').click(function () { callRestAPI() });
    // Perform an asynchronous HTTP (Ajax) API request.
    function callRestAPI() {
        var root = 'https://httpbin.org';
        $.ajax({
            url: root + '/get',
            method: 'GET'
        }).then(function (response) {
            console.log(response);
            $('#showResult').html(response.url);
        });
    }
})($);